package com.alibaba.util;

import com.alibaba.config.DBConfig;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBHelper {

    private static Connection connection = null;

    static {
        try {
            Class.forName(DBConfig.DRIVER);
            connection = DriverManager.getConnection(DBConfig.URL, DBConfig.USER, DBConfig.PASSWORD);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Connection getConnection() {
        if(connection == null) {
            try{
                Class.forName(DBConfig.DRIVER);
                connection = DriverManager.getConnection(DBConfig.URL, DBConfig.USER, DBConfig.PASSWORD);
                return connection;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return connection;
    }
}
