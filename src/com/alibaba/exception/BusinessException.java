package com.alibaba.exception;

public class BusinessException extends Exception{

    private int code;

    private String msg;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public BusinessException(int code, String msg){
        super(msg);
        this.code = code;
        this.msg = msg;
    }
    public BusinessException(int code, String msg, Throwable e){
        super(msg, e);
        this.code = code;
        this.msg = msg;
    }

    public BusinessException(String msg){
        super(msg);
        this.msg = msg;
    }

    public BusinessException(){
        super("业务异常");
    }
    public BusinessException(ErrorCode errorCode){
        super(errorCode.getMsg());
        this.code = errorCode.getCode();
        this.msg = errorCode.getMsg();

    }
}
