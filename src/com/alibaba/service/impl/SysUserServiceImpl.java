package com.alibaba.service.impl;

import com.alibaba.exception.BusinessException;
import com.alibaba.service.SysUserService;

import java.sql.SQLException;

public class SysUserServiceImpl implements SysUserService {

        private final SysUserDAO sysUserDAO = new SysUserDAOImpl();

        @Override
        public boolean login(String username, String password) throws BusinessException {

                try {
                        SysUserEntity entity = sysUserDAO.queryByUsername(username);
                        if (entity == null) {
                            throw new RuntimeException(ErrorCode.USER_IS_NOT_EXISTS);
                        }
                } catch (SQLException e) {
                        e.printStackTrace();
                }
                return false;
        }
}
